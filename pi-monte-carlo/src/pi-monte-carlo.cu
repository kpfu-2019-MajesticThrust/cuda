/*
 ============================================================================
 Name        : pi-monte-carlo.cu
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : PI computation via Monte-Carlo method
 ============================================================================
 */

#include <iostream>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdlib.h>
#include <cstdio>

// error checking macros

static void CheckCudaErrorAux(const char *, unsigned, const char *,
		cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)


__device__ void cudaRand(float *d_out) {
	int i = blockDim.x * blockIdx.x + threadIdx.x;

	curandState state;
	curand_init((unsigned long long) clock() + i, 0, 0, &state);

	*d_out = curand_uniform(&state);
}

/**
 * Generate n points inside a [0,1] quadrant, calculate how many points
 * are inside the unit circle and add it to d_insideN.
 */
__global__ void unitCircleScatter(unsigned int n, unsigned int* d_insideN) {
	printf("%d", threadIdx.x);
	unsigned int insideN = 0;
	for (size_t i = 0; i < n; i++) {
		float* x;
		float* y;
		cudaRand(x);
		cudaRand(y);

		if ((*x) * (*x) + (*y) * (*y) <= 1) {
			insideN++;
		}
	}
	atomicAdd(d_insideN, insideN);
}

int main(void) {
	// TODO determine automatically
	const unsigned int blockCount = 32;
	// TODO determine automatically
	const unsigned int threadCount = 10;
	// count of random points per thread
	const unsigned int nPerThread = 1;

	const unsigned long nPoints = blockCount * threadCount * nPerThread;

	unsigned int insideN = 0;
	unsigned int* d_insideN;
	CUDA_CHECK_RETURN(cudaMalloc((void** )&d_insideN, sizeof(unsigned int)));
	CUDA_CHECK_RETURN(cudaMemcpy(d_insideN, &insideN, sizeof(unsigned int), cudaMemcpyHostToDevice));
	unitCircleScatter<<<blockCount, threadCount>>>(nPerThread, d_insideN);
	cudaError_t err = cudaGetLastError();
	//if (err != cudaSuccess) {
		printf("%s", cudaGetErrorString(err));
	//}
	getchar();
	CUDA_CHECK_RETURN(cudaMemcpy(&insideN, d_insideN, sizeof(unsigned int), cudaMemcpyDeviceToHost));

	const float pi = insideN / nPoints * 4;

	std::cout << blockCount * threadCount * nPerThread
			<< " random points; pi = " << pi << std::endl;

	// cleanup
	CUDA_CHECK_RETURN(cudaFree(d_insideN));

	//std::cin.get();
	

	return 0;
}

/**
 * Check the return value of the CUDA runtime API call and exit
 * the application if the call has failed.
 */
static void CheckCudaErrorAux(const char *file, unsigned line,
		const char *statement, cudaError_t err) {
	if (err == cudaSuccess)
		return;
	std::cerr << statement << " returned " << cudaGetErrorString(err) << "("
			<< err << ") at " << file << ":" << line << std::endl;
	exit(1);
}

