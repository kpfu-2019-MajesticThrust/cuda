# Вычисление числа Пи методом Монте-Карло
# разбор алгоритма на питоне

import random

def pi_monte_carlo(n: int) -> float:
    inside_n = 0

    for _ in range(n):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)

        # если за пределами единичного круга
        # квадратный корень не обязателен
        if x**2 + y**2 <= 1:
            inside_n += 1

    return inside_n / n * 4

if __name__ == "__main__":
    a = 5000
    b = 200000
    step = 5000
    for n in range(a, b + 1, step):
        print("{} points: pi = {}".format(n, pi_monte_carlo(n)))
